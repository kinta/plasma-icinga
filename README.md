# Plasma icinga (both dataengine and applet)

Further information could be found in src/dataengine and src/applet.

## Build and install

Will add a dataengine and an applet to view the collected data coming from an icinga server.

To build:

```
$ mkdir build
$ cd build
$ cmake ../ -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_BUILD_TYPE=Release -DKDE_INSTALL_USE_QT_SYS_PATHS=ON
$ make
```

to install:

```
make install 
```

with ninja build system generator:

```
mkdir build
cd build
/usr/bin/cmake -GNinja -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_BUILD_TYPE=Release -DKDE_INSTALL_USE_QT_SYS_PATHS=ON ..
sudo ninja install
```

## Run

Restart plasma to load the applet 
(in a terminal type: 
`kquitapp plasmashell`
and then
`plasmashell`)

or view it with 
`plasmoidviewer -a icinga_monitor`

It could be easily debugged launching within qtcreator.
In left bar pick projects >> and in run settings:

It could be easily debugged launching within qtcreator.
In left bar pick projects >> and in run settings:

- Executable field: `/usr/bin/plasmoidviewer`
- Command line arguments: `-a .  -l topedge  -x 0 -y 0 -s 800x600`
- Working directory: `%{ActiveProject:Path}/src/applet/package`



# Debugging

With the icinga_monitor applet, run:

`QT_LOGGING_RULES="*.debug=false;kde.dataengine.icinga.debug=true;" plasmashell`

or

`QT_LOGGING_RULES="*.debug=false;kde.dataengine.icinga.debug=true;" plasmoidviewer`

