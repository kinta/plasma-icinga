// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL 
// SPDX-FileCopyrightText: 2020 Aleix Quintana Alsius <kinta@communia.org>
import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import org.kde.plasma.components as PC3
import org.kde.kirigami as Kirigami
import org.kde.kcmutils as KCM

KCM.SimpleKCM {
    id: page
    width: childrenRect.width
    height: childrenRect.height

    property alias cfg_icingaUrl: icingaUrl.text
    property alias cfg_icingaApiUser: icingaApiUser.text
    property alias cfg_icingaApiKey: icingaApiKey.text
    property alias cfg_icingaApiRequestsInterval: intervalSpinbox.value
    property alias cfg_width: widthSpinBox.value
    property alias cfg_height: heightSpinBox.value
    property alias cfg_compactCriticalQuantity: compactCriticalQuantity.checked

    Kirigami.FormLayout {
        //anchors.left: parent.left
        //anchors.right: parent.right

        TextField {
            id: icingaUrl
            Kirigami.FormData.label: i18n("Icinga Url:")
            placeholderText: i18n("https://youricinga.org")
            ToolTip.text:i18n("Icinga server url as in https://icingaserver.org:5665")
            ToolTip.visible: hovered
        }
        TextField {
            id: icingaApiUser
            Kirigami.FormData.label: i18n("Icinga API user:")
            placeholderText: i18n("User name")
        }
        TextField {
            id: icingaApiKey
            Kirigami.FormData.label: i18n("Icinga API token:")
            placeholderText: i18n("API token")
        }
        RowLayout {
            Label {
                text: i18n("Refer to %1 to configure your icinga API", "https://icinga.com/docs/icinga2/latest/doc/12-icinga2-api/#authentication")
            }
        }
        RowLayout {
            Label {
                text: i18n("Refresh interval in ms :")
            }
            SpinBox {
                id: intervalSpinbox
                from: 0
                to: 2147483647 // 2^31-1

            }
        }
        RowLayout {
            Kirigami.FormData.label: i18n("Size:")
            SpinBox {
                id: widthSpinBox
                from: 0
                to: 2147483647 // 2^31-1
            }
            Label {
                text: " x "
            }
            SpinBox {
                id: heightSpinBox
                from: 0
                to: 2147483647 // 2^31-1
            }
        }
        RowLayout {
            PC3.CheckBox {
                id: compactCriticalQuantity
                text: i18n("Show critical quantity in compact representation:")
            }
        }
    }
}
