import QtQuick
import QtQuick.Layouts 1.3
import org.kde.plasma.plasmoid
import org.kde.plasma.core as PlasmaCore
import org.kde.plasma.plasma5support as P5Support
import org.kde.plasma.components as PlasmaComponents
import org.kde.notification

import org.kde.kirigami as Kirigami
import org.kde.kitemmodels

//import QtQuick.Controls 2.15 as Controls
import QtQuick.Controls 2.15



Repeater{
    delegate:
        ColumnLayout {
        anchors.fill: parent
        Layout.alignment: Qt.AlignHCenter
        visible: model.errorMessage.length > 1
        Kirigami.Icon {
            id: errorIcon
            Layout.alignment: Qt.AlignHCenter
            Layout.preferredWidth: Kirigami.Units.iconSizes.huge
            Layout.preferredHeight: width
            source: {
                // icingaDataSource.data ===  model.errorMeassage
                if (model.errorMessage.length > 1){
                    tooltip = model.errorMessage
                    return "network-disconnect"
                } else {
                    return "network-connect"
                }
            }
        }
        /* Verbose visual errors applet...
                 * To perform what was possible with:
                 * Plasmoid.configurationRequiredReason: fetchError? errorString : i18n("Plasmoid needs Icinga API credentials")
                 */
                Label {
                    id: error
                    Layout.fillWidth: true
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignHCenter
                    visible: model.errorMessage.length > 1
                    text: "Error connecting to " + model.DataEngineSource + " :\n" + model.errorMessage
                    /*{
                    console.debug(JSON.stringify({
                    "concreteError": icingaDataSource.data[model["DataEngineSource"]]['errorMessage'],
                    "concreteErrorSource:" : model.DataEngineSource }))
                    return "Error connecting to " + model.DataEngineSource + " :\n" + model.errorMessage
                    }*/
                }

    }
}
