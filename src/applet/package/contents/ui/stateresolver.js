.pragma library

var IcingaStates = {
    "Services": {
        0 : {"text" : "Ok" , "color" : "#4b7", "icon" : "data-success", "iconEmblem": ""},
        1 : {"text" :"Warning", "color" : "#fa4", "icon" : "data-warning", "iconEmblem": ""},
        4 : {"text" : "WarningHandled", "color" : "#fc6", "icon" : "data-warning", "iconEmblem": "emblem-pause"},
        2 : {"text" : "Critical", "color" : "#f56", "icon" : "data-error", "iconEmblem": ""},
        5 : {"text" : "CriticalHandled", "color" : "#f9a", "icon" : "data-error", "iconEmblem": "emblem-pause"},
        3 : {"text" : "Unknown", "color" : "#a4f", "icon" : "question", "iconEmblem": ""},
        6 : {"text" : "UnknownHandled", "color" : "#c7f", "icon" : "question", "iconEmblem": "emblem-pause"},
    },
    "Hosts": {
        0 : {"text" : "Up", "color" : "#4b7"},
        1 : {"text" : "Down", "color" : "#f56"},
        3 : {"text" : "DownHandled", "color" : "#f9a"},
        2 : {"text" : "Unreachable", "color" : "#a4f"},
        4 : {"text" : "UnreachableHandled", "color" : "#c7f"},
    },
    "PerfData": ['value','warnings','critical','min_value','max_value']
}
