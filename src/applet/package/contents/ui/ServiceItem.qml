// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2020 Aleix Quintana Alsius <kinta@communia.org>
import QtQuick
import QtQuick.Layouts
import Qt.labs.qmlmodels

import QtQuick.Controls
import QtQml.Models

import org.kde.plasma.core as PlasmaCore
import org.kde.plasma.extras as PlasmaExtras
import org.kde.plasma.components as PC3

import org.kde.kquickcontrolsaddons
import org.kde.kirigami as Kirigami
import org.kde.quickcharts as Charts
import "stateresolver.js" as StateResolver
import "icinga.js" as Icinga

PlasmaExtras.ExpandableListItem {

    id: serviceItem
    // It's awaiting for a data props as:
    //    "displayName: " + displayName + "\n" +
    //    "serviceId: " + serviceId + "\n" +
    //    "hostName: " + hostName + "\n" +
    //    "lastCheckOutput: " + lastCheckOutput + "\n" +
    //    "zone: " + zone + "\n" +
    //    "service state: " + serviceState + "\n" +
    //    "raw: " + JSON.stringify(raw, null, 2) + "\n"

    property string status : serviceState
    property string lastCheck : raw.last_check
    property var comments

    //background: Kirigami.Theme.backgroundColor
    //highlighted: ListView.isCurrentItem

    title: displayName + " on " + hostName
    subtitle: (zone)? hostName + " from zone:" + zone + " last check: " + milisToString(lastCheck) : hostName
    //subtitleColor: StateResolver.IcingaStates["Services"][status].color
    icon: StateResolver.IcingaStates["Services"][status].icon
    iconEmblem: StateResolver.IcingaStates["Services"][status].iconEmblem
    isBusy: true
    showDefaultActionButtonWhenBusy: true
    defaultActionButtonAction: Action {
        id: moreInfoButton
        enabled: plasmoid.configuration.icingaWebUrl.length !== ""
        icon.name: "link"
        text: i18n("Web")
        onTriggered: {
            Qt.openUrlExternally( "%1/monitoring/service/show?host=%2&service=%3".
                                 arg(plasmoid.configuration.icingaWebUrl)
                                 .arg(hostName)
                                 .arg(displayName))
        }
    }
    contextualActions: [
        Action {
            enabled: true
            text: i18n("More information")
            icon.name: "showinfo"
            onTriggered: {
                overlayDialogBody = JSON.stringify(
                            raw,
                            null, 2)
                pageStack.push(overlayDialog, {
                                   "id": overlay
                               })
            }
        },
        Action {
            enabled: true
            text: i18n("Recheck")
            icon.name: "view-refresh"
            onTriggered: {
                Icinga.rescheduleCheck(displayName, hostName, function(response){
                    //console.log(JSON.stringify(response))
                    if (response.results[0]?.code === 200) {
                        notificationComponent.createObject(root, {})?.setTitle("Rescheduling check").sendEvent();
                        console.log(JSON.stringify(response))
                        serviceItem.isBusy = true
                    } else {
                        notificationComponent.createObject(parent, {title = i18n("Could not reschedule the check")})?.sendEvent();
                    }
                })
            }
        }
    ]
    onLastCheckChanged: { isBusy = false }

    customExpandedViewContent: detailsComponent

    Component {
        id: detailsComponent
        Column {
            spacing: Kirigami.Units.smallSpacing
            property Item detailsTabBar: detailsTabBar
            PC3.TabBar {
                id: detailsTabBar

                anchors {
                    left: parent.left
                    right: parent.right
                }
                height: visible ? implicitHeight : 0
                implicitHeight: contentHeight
                position: PC3.TabBar.Header
                onCurrentIndexChanged: {
                }

                PC3.TabButton {
                    id: speedTabButton
                    text: i18n("Details")
                }

                PC3.TabButton {
                    id: detailsTabButton
                    text: i18n("Performance data")
                }

                PC3.TabButton {
                    id: commentsTabButton
                    text: i18n("Comments")
                    onPressed: {
                        Icinga.comments(displayName, hostName,function(data){
                            console.log(JSON.stringify(data));
                            serviceItem.comments = data.results
                        })
                    }
                }
                Component.onCompleted: {
                    //currentIndex = 1;

                }
            }
            MouseArea {
                width: 300
                height: detailsGrid.implicitHeight
                visible: detailsTabBar.currentIndex === 0
                activeFocusOnTab: lastCheckOutput.length > 0
                acceptedButtons: Qt.RightButton

                onPressed: mouse => {
                               contextMenu.show(mouse.x, mouse.y);
                           }

                Clipboard {
                    id: clipboard
                }

                PlasmaExtras.Menu {
                    id: contextMenu
                    property string text

                    function show(x, y) {
                        contextMenu.text = lastCheckOutput
                        open(x, y)
                    }

                    PlasmaExtras.MenuItem {
                        text: i18n("Copy")
                        icon: "edit-copy"
                        enabled: lastCheckOutput !== ""
                        onClicked: clipboard.content = lastCheckOutput.text
                    }
                }


                GridLayout {
                    id: detailsGrid
                    width: parent.width
                    columns: 1
                    rowSpacing: Kirigami.Units.smallSpacing / 4

                    Repeater {
                        model: 1
                        Label {
                            id: view
                            Layout.fillWidth: true
                            text: lastCheckOutput
                            textFormat: Text.AutoText
                            Accessible.description: lastCheckOutput
                            wrapMode: Text.WordWrap
                            onLinkActivated: Qt.openUrlExternally(link)
                            font.pointSize:Kirigami.Theme.smallFont.pointSize *0.7
                            font.family: "monospace"
                        }
                    }
                }
            }
            ColumnLayout {
                visible: detailsTabBar.currentIndex === 1

                GridLayout {
                    columns: 4
                    uniformCellWidths: true
                    Repeater {
                        model: raw['last_check_result']['performance_data']
                        PerformanceGauge {
                        }


                    }

                }
                PerformanceDataTable {
                    tableData: raw['last_check_result']['performance_data']
                }
            }
            ColumnLayout {
                visible: detailsTabBar.currentIndex === 2
                width:parent.width
                Button {
                    text: qsTr("Comment/handle")
                    Layout.alignment: Qt.AlignHCenter
                    onClicked: commentDialog.open()
                }
                Repeater {
                    model: serviceItem.comments
                    RowLayout{
                        width: parent.width
                        Kirigami.InlineMessage {
                            id: comment_msg
                            property string comment_type: serviceItem.comments[index].attrs.entry_type === '4' ? "ack":"comment"
                            Layout.fillWidth: true
                            visible: true
                            icon.source: comment_type == 'ack'? "dialog-positive":"imagecomment"
                            text: serviceItem.comments[index].attrs.text
                            onLinkActivated: link => Qt.openUrlExternally(link)
                            actions: [
                                Kirigami.Action {

                                    icon.name: "delete"
                                    text: comment_msg.comment_type == 'ack' ? qsTr("remove acknowledge") : qsTr("remove comment")
                                    onTriggered: {
                                        const comment_name = serviceItem.comments[index].name
                                        const un_action = comment_msg.comment_type == 'ack' ? Icinga.unAcknowledge:Icinga.unComment
                                        un_action(comment_name, displayName, hostName, function(response){
                                            //console.log(JSON.stringify(response))
                                            if (response.results[0]?.code === 200) {
                                                console.log(JSON.stringify(response))
                                                serviceItem.isBusy = comment_msg.comment_type == 'ack'
                                                Icinga.comments(displayName, hostName,function(data){
                                                    console.log(JSON.stringify(data));
                                                    serviceItem.comments = data.results
                                                })
                                            } else {
                                                notificationComponent.createObject(parent, {title = i18n("Could not remove comment or acknowledge")})?.sendEvent();
                                            }
                                        })
                                    }

                                }
                            ]
                        }
                    }
                }
            }
        }
    }
    Kirigami.PromptDialog {
        id: commentDialog
        title: "Comment or Acknowledge"
        height: Kirigami.Units.gridUnit * 10
        y: root.y

        standardButtons: Kirigami.Dialog.NoButton
        customFooterActions: [
            Kirigami.Action {
                text: qsTr("Comment")
                icon.name: "imagecomment"
                onTriggered: {
                    Icinga.comment(commentText.text, displayName, hostName, function(response){
                        //console.log(JSON.stringify(response))
                        if (response.results[0]?.code === 200) {
                            refreshComments()
                        } else {
                            notificationComponent.createObject(parent, {title = i18n("Could not comment")})?.sendEvent();
                        }
                    })
                    commentDialog.close();
                }
            },
            Kirigami.Action {
                text: qsTr("Acknowledge")
                icon.name: "view-time-schedule-edit"
                onTriggered: {
                    Icinga.acknowledge(commentText.text, displayName, hostName, function(response){
                        if (response.results[0]?.code === 200) {
                            serviceItem.isBusy = true
                            refreshComments()
                        } else {
                            notificationComponent.createObject(parent, {title = i18n("Could not acknowledge")})?.sendEvent();
                        }
                    })
                    commentDialog.close();
                }
            },
            Kirigami.Action {
                text: qsTr("Cancel")
                icon.name: "dialog-cancel"
                onTriggered: {
                    commentDialog.close();
                }
            }
        ]
        TextArea {
            id: commentText
            placeholderText: qsTr("Comment or Acknowledge...")
        }
    }
    function refreshComments() {
        Icinga.comments(displayName, hostName,function(data){
            serviceItem.comments = data.results
        })
    }
    function milisToString(sec) {
        let milis = sec * 1000
        var date = new Date(milis)
        let b = Qt.formatDate(date ,  Qt.ISODate);
        return date.toLocaleString(Qt.locale(), "hh:mm:ss dd/MM")
    }
}

/*##^##
 * Designer {
 *    D{i:0;formeditorZoom:3}
 * }
 * ##^##*/
