import QtQuick
import QtQuick.Layouts
import Qt.labs.qmlmodels

import QtQuick.Controls
import QtQml.Models

import org.kde.plasma.core as PlasmaCore
import org.kde.plasma.extras as PlasmaExtras
import org.kde.plasma.components as PC3

import org.kde.kquickcontrolsaddons
import org.kde.kirigami as Kirigami
import org.kde.quickcharts as Charts
import "stateresolver.js" as StateResolver
import "icinga.js" as Icinga

ColumnLayout {
    id: performanceGauge
    property var performanceResults: modelData
    PlasmaExtras.Heading{
        level:4
        Layout.fillWidth: true
        text: prfDataItem.name
    }
    Item {
        width: Kirigami.Units.gridUnit * 5
        height:Kirigami.Units.gridUnit * 5


        PlasmaExtras.Heading{
            level:1
            Layout.fillWidth: true
            anchors.centerIn: parent
            text: prfDataItem.perfdata[0]
            color: prfDataItem.colorSource.value
        }

        Charts.PieChart {
            /**
             * Tipically will analize as in https://icinga.com/docs/icinga-2/latest/doc/05-service-monitoring/#performance-data-metrics
             * and in and in https://www.monitoring-plugins.org/doc/guidelines.html#AEN201
             * these results:
             * security_updates=0;;;
             * defined as value;warnings;critical;min_value;max_value
             * If no warnings or criticals defined it is a simple counter
             * It could be a warning counter (without critical defined)
             * It could be a critical counter (without warning defined)
             * It could be an unknown counter (with value but without critical or warning defined)
             */
            id: prfDataItem
            property var perfdata: performanceResults.split('=')[1].split(";").map(item => { return item.replace(/[^0-9\.]+/g, "")}).map(Number)
            property string name: performanceResults.split('=')[0]
            visible: perfdata[1] || perfdata[2]
            anchors.fill: parent
            range {
                from: 0;
                to: {
                    if (perfdata[0] == 0) return 1;
                    return perfdata[4] || perfdata[2] || perfdata[1] || perfdata[0] //?perfdata[4]:perfdata[2]?perfdata[2]:perfdata[];
                }
                automatic: false
            }
            smoothEnds: true
            fromAngle:-135
            toAngle: 135
            thickness: 5
            backgroundColor: 'black'

            colorSource: Charts.SingleValueSource { value: {
                    let color = PlasmaCore.Theme.textColor
                    if ((prfDataItem.perfdata[1] || prfDataItem.perfdata[2]) &&
                        ((prfDataItem.perfdata[0] < prfDataItem.perfdata[1]) || (prfDataItem.perfdata[0] < prfDataItem.perfdata[2]))){
                        // Crit or Warn defined and value less than crit or warn
                        color = "green"
                    } else if (prfDataItem.perfdata[2] && prfDataItem.perfdata[0] >= prfDataItem.perfdata[2]){
                        // Crit defined and value more or eq than crit
                        color = "red"
                    } else if (prfDataItem.perfdata[1] && prfDataItem.perfdata[0] >= prfDataItem.perfdata[1]){
                        // Warn defined and value more or eq than warn
                        color = "orange"
                    } else if (prfDataItem.name === 'Unknown' && prfDataItem.perfdata[0] > 0) {
                        // Unknown name and value more than 0
                        color = 'purple'
                    }

                    return color
                }
            }
            nameSource: Charts.SingleValueSource { value: "First" }

            valueSources: [
                Charts.SingleValueSource{ value: prfDataItem.perfdata[0] }
            ]

        }
    }
}
