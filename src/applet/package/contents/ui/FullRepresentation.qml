// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2020 Aleix Quintana Alsius <kinta@communia.org>
import QtQuick
import QtQuick.Layouts
import org.kde.plasma.core  as PlasmaCore
import org.kde.plasma.components as PlasmaComponents

import org.kde.kirigami as Kirigami
import org.kde.kitemmodels

import QtQuick.Controls
import QtCharts

import "stateresolver.js" as StateResolver

Kirigami.ScrollablePage {
    id: page
    title: "Icinga monitor"
    implicitHeight: Kirigami.Units.gridUnit * 28
    implicitWidth: Kirigami.Units.gridUnit * 28
    Layout.preferredWidth: plasmoid.configuration.width
    Layout.preferredHeight: plasmoid.configuration.height
    topPadding: 0
    leftPadding: 0
    rightPadding: 0

    property string overlayDialogBody: "No data"

    property alias pie : chart

    Component {
        id: overlayDialog
        ColumnLayout {
            spacing: 0
            Button {
                Layout.fillWidth: true
                text: qsTr("Close")
                Layout.alignment: Qt.AlignHCenter
                onClicked: pageStack.pop()
            }
            ScrollView {
                Layout.fillWidth: true
                Layout.fillHeight: true

                Label {
                    wrapMode: Text.WordWrap
                    text: overlayDialogBody
                }
            }
        }
    }
    header:             PlasmaComponents.TabBar {
        id: bar
        currentIndex: 0
        Layout.fillWidth: true
        Layout.fillHeight: true
        PlasmaComponents.TabButton {
            text: servicesPage.title
        }
        PlasmaComponents.TabButton {
            text: hostsPage.title
        }
        PlasmaComponents.TabButton {
            text: tacticalPage.title
        }
    }

    //QQC2
    StackView {
        id: pageStack
        anchors.fill: parent
        clip: true
        initialItem: ColumnLayout {
            anchors.fill: parent


            Page{
                visible: bar.currentIndex === 0
                Layout.fillWidth: true
                Layout.fillHeight: true
                footer:                         Label {
                    horizontalAlignment: Qt.AlignRight
                    text: listServices.count + " items"
                }
                header:                         ToolBar {
                    id: toolbar
                    Layout.fillWidth: true
                    RowLayout {
                        Kirigami.SearchField {
                            id: servicesSearchField
                            Layout.alignment: Qt.AlignHCenter
                            focus: true
                            cursorVisible: false
                            Layout.maximumWidth: Kirigami.Units.gridUnit*30
                        }
                        ComboBox {
                            id: servicesGroupBy
                            model: ["serviceState", "zone", "hostName"]
                            implicitWidth: Kirigami.Units.gridUnit * 4
                            ToolTip.text: "Group by " + currentText
                            ToolTip.visible: hovered
                        }
                        /* TODO Include certain  states only
                                Button {
                                   //text: qsTr("Sort List View")
                                   onClicked: sortListDialog.open()
                                   implicitWidth: Kirigami.Units.gridUnit * 2
                                   icon.name: "view-filter"
                                }
                            */
                        Button {
                            //text: qsTr("Sort List View")
                            id: servicesOrder
                            implicitWidth: Kirigami.Units.gridUnit * 2
                            icon.name: checked? "view-sort-descending" : "view-sort-ascending"
                            checkable: true
                        }
                        Button {
                            id: servicesRefresh
                            visible: plasmoid.configuration.icingaApiRequestsInterval === 0
                            implicitWidth: Kirigami.Units.gridUnit * 2
                            icon.name: "view-refresh"
                            onClicked: {
                                icingaDataSource.restart()
                            }

                        }
                        Dialog {
                            id: sortListDialog
                            property var serviceStates : {
                                var services = Object.keys(StateResolver.IcingaStates.Services)
                                /*(let i = 0; i < services.length; i++) {
                                        services[i] = true
                                    }*/
                            }
                            modal: true
                            focus: true
                            x: (servicesPage.width - width) / 2
                            y: servicesPage.height / 2 - height
                            width: Math.min(servicesPage.width - Kirigami.Units.gridUnit * 4, Kirigami.Units.gridUnit * 20)
                            height: Kirigami.Units.gridUnit * 20
                            standardButtons: Dialog.Close
                            title: qsTr("Radio List Items")
                            contentItem: ScrollView {
                                ListView {
                                    model: { return Object.keys(StateResolver.IcingaStates.Services)}
                                    delegate: SwitchDelegate {
                                        width: parent.width
                                        text: (StateResolver.IcingaStates.Services[index].text)
                                        checked: true
                                        onCheckedChanged: {
                                            console.log(JSON.stringify(model, null, 2))
                                            console.log(index + ":" +checked)
                                        }
                                    }
                                }
                                Component.onCompleted: background.visible = true
                            }
                            onRejected: {
                            }
                        }
                    }
                }

                ScrollView {
                    id: servicesPage
                    property string title: qsTr("Services")
                    anchors.fill: parent
                    ScrollBar.horizontal.policy: Qt.ScrollBarAlwaysOff
                    ListView {
                        id: listServices
                        model:servicesFiltersModel_thirdLevel
                        delegate: ServiceItem{
                            activeFocusOnTab: true
                        }

                        displaced: Transition {
                            YAnimator {
                                duration: Kirigami.Units.longDuration
                                easing.type: Easing.InOutQuad
                            }
                        }
                        section {
                            id: servicesSections
                            property: servicesGroupBy.currentText//"zone"
                            delegate: Kirigami.ListSectionHeader {
                                width: listServices.width
                                label: {
                                    switch (servicesSections.property){
                                    case "serviceState":
                                        if (StateResolver.IcingaStates.Services.hasOwnProperty(section)) return StateResolver.IcingaStates.Services[section].text
                                        break;
                                    default:
                                        return section
                                    }
                                }
                            }
                        }
                        KSortFilterProxyModel {
                            id: servicesFiltersModel_thirdLevel
                            filterRoleName:"serviceState"
                            filterString:""
                            sourceModel:
                                KSortFilterProxyModel {
                                id: servicesFiltersModel_secondLevel
                                filterRoleName:"zone"
                                filterString:""
                                sourceModel: KSortFilterProxyModel {
                                    // 2depth tuple access
                                    id: servicesFiltersModel_firstLevel
                                    filterRoleName: "id"
                                    filterRegularExpression:{
                                        if (servicesSearchField.text === "") return new RegExp()
                                        return new RegExp("%1".arg(servicesSearchField.text), "i")
                                    }
                                    sortRoleName: servicesGroupBy.currentText
                                    sortOrder: servicesOrder.checked? Qt.AscendingOrder : Qt.DescendingOrder
                                    recursiveFilteringEnabled: true
                                    sourceModel: root.servicesSearchSortFilterModel?root.servicesSearchSortFilterModel:null
                                }
                            }
                        }
                    }
                }

            }
            ScrollView {
                visible: bar.currentIndex == 1
                id: hostsPage
                property string title: qsTr("Hosts")
                Layout.fillWidth: true
                Layout.fillHeight: true
                ListView {
                    id: listServiceHosts
                    model: hostsModel
                    delegate: Label {
                        //required property string modelData
                        text: {
                            return icingaDataSource.data[modelData]['hosts'][index]
                        }
                        anchors.leftMargin: 2
                    }
                    highlight: Rectangle {
                        color: "lightblue"
                        Layout.fillWidth: true
                    }
                }
                Rectangle {
                    color: "blue"
                }
            }
            Page{
                visible: bar.currentIndex == 2
                id: tacticalPage
                title: "Tactical"
                Layout.fillWidth: true
                Layout.fillHeight: true
                PieView {
                    id: chart
                    anchors.fill: parent
                }
            }

        }
    }
    /* Plasmoidviewer fuzzes category config menu, make it unusable... This part will allow to launch menu in plasmoidwindowed
Button {
    id: hoverArea
    //anchors.fill:parent
    onClicked: {
        menu.open()
    }
    PlasmaComponents.ContextMenu {
        id: menu
        PlasmaComponents.MenuItem {
            property QtObject configureAction: null

            enabled: configureAction && configureAction.enabled
            visible: configureAction && configureAction.visible

            text: configureAction ? configureAction.text : ""
            icon: configureAction ? configureAction.icon : ""

            onClicked: configureAction.trigger()

            Component.onCompleted: configureAction = plasmoid.action("configure")
        }
    }
}*/
}
