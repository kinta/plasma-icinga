﻿// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2020 Aleix Quintana Alsius <kinta@communia.org>
import QtQuick
import QtQuick.Layouts 1.3
import org.kde.plasma.plasmoid
import org.kde.plasma.core as PlasmaCore
import org.kde.plasma.plasma5support as P5Support
import org.kde.plasma.components as PlasmaComponents
import org.kde.notification

import org.kde.kirigami as Kirigami
import org.kde.kitemmodels

//import QtQuick.Controls 2.15 as Controls
import QtQuick.Controls 2.15



PlasmoidItem {
    id:root
    property var servicesSearchSortFilterModel
    property int counterServicesOk : 0
    property int counterServicesWarning : 0
    property int counterServicesWarningHandled : 0
    property int counterServicesCritical : 0
    property int counterServicesCriticalHandled : 0
    property int counterServicesUnknown : 0
    property int counterServicesUnknownHandled : 0
    property bool fetchError : errorModel.count

    property string tooltip: "Waiting for data"

    function dbg(ob, name){
        console.debug("----" + name + "------")
        console.debug("[DBG]", JSON.stringify(ob, null, 2));
        console.debug("[DBG] Trace:")
        console.trace()
        //for (var p in data){ console.log(p + ": " + data[p]); }
    }
    Timer {
        id: servicesFetchTimer
        running: false
        repeat: true
        onTriggered: {
            // Normalize the interval to a reasonable time
            interval = Plasmoid.configuration.icingaApiRequestsInterval
            var service = icingaDataSource.serviceForSource("services")
            var operation = service.operationDescription("fetchServices")
            operation["icingaUrl"] = Plasmoid.configuration.icingaUrl
            operation["icingaApiUser"] = Plasmoid.configuration.icingaApiUser
            operation["icingaApiKey"] = Plasmoid.configuration.icingaApiKey

            const serviceJob = service.startOperationCall(operation);
            //dbg(service, "SERVICE");
            //dbg(serviceJob);
            //dbg(icingaDataSource);
            //dbg(activeUpdateModelsData);
            serviceJob.finished.connect(activeUpdateModelsData)

            function activeUpdateModelsData(job) {
                if (!job.result){
                    // job not succesful, but is handled in error datamodel below
                }

                // This will only work for updates done via serviceForSource, not when configured globally with icingarc configfile
                // Some models are not ready to obtain data from some models, TODO make it ready as services do...
                //dbg("ServiceJob result: " + job.result + " op: " + job.operationName);
                //hosts = icingaDataSource.data["https://monitor.icinga:5665/v1/objects/services"].hosts
                //hosts_up = icingaDataSource.data["https://monitor.icinga:5665/v1/objects/services"].hostsCount
                // Set hosts:
                //hosts = icingaDataSource.data["https://monitor.icinga:5665/v1/objects/services"].hosts
            }
        }
    }


    /***
    * Will start a timer to upgrade the dataengine based on interval set
    *
    * Maybe will be better to start the timer to update the plasmaengine data directly within this data source
    */
    P5Support.DataSource {
        id: icingaDataSource
        property var icingaObjects : [ "services" ] //TODO
        //^ expand to "hosts", "hostgroups", "servicegroups" ... https://icinga.com/docs/icinga2/latest/doc/12-icinga2-api/#icinga2-api-config-objects-query
        // whenever it is implemented in dataengine.
        engine: "icinga"
        connectedSources: [ "services" ]
        //interval: plasmoid.configuration.icingaApiRequestsInterval
        //intervalAlignment: PlasmaCore.Types.AlignToMinute
        onSourceConnected: source => {
                               // First time trigger
                               if ( icingaObjects.includes(source) ) {
                                   console.debug("Connecting source to request service")
                                   // this is the query about what icingaObjects are available
                                   // that is icinga2 api objects query implementations. Will start
                                   // timer if
                                   //if (plasmoid.configuration.icingaUrl !== "" && plasmoid.configuration.icingaApiUser !== "" &&  plasmoid.configuration.icingaApiKey !== "" ){
                                   // If operating via serviceForSource instead of setting icingarc
                                   servicesFetchTimer.running = true
                                   //}
                                   // TODO pause timer if cannot connect, and let start manually...
                               } else {
                                   console.debug("Connected to configured Url to Request Data for " + source)
                                   // Once a new Endpoint is connected let's link that model to Model that feeds the view
                                   servicesSearchSortFilterModel = this.models[source]
                                   servicesCountersModel.filterSource = source
                                   errorDataModel.sourceFilter = source
                               }
                           }
        onSourceDisconnected: {
        }

        onSourceAdded: source => {
                           disconnectSource(source);
                           connectSource(source)
                       }
        onSourceRemoved: source => { disconnectSource(source)}
        function restart(){
            connectedSources = []
            connectedSources = ["services", Plasmoid.configuration.icingaUrl ]
        }
    }


    // TODO filter as it should to pick data from model instead of ugly and not smart refreshed arrays
    P5Support.DataModel {
        id: hostsDataModel
        dataSource: icingaDataSource
        keyRoleFilter: "hosts"
    }

    KSortFilterProxyModel {
        // 2depth tuple access
        id: hostsModel
        filterRoleName:"hosts"
        filterRegularExpression: RegExp(".*")
        sourceModel: hostsDataModel
    }



    KSortFilterProxyModel {
        id: errorModel
        filterRoleName: "errorMessage"
        sourceModel:P5Support.DataModel {
            id: errorDataModel
            dataSource: icingaDataSource
            //sourceFilter: // No specified means everything (dynamic set above^)
        }

        onDataChanged: data => {
                           //errorString = sourceModel.get(0).errorMessage
                           fetchError = sourceModel.get(0).errorMessage.length > 0
                       }
    }

    P5Support.DataModel {
        id: servicesCountersDataModel
        dataSource: icingaDataSource
        keyRoleFilter: "counters"
    }

    // TODO filter as it should to pick data from model instead of ugly and not smart refreshed arrays
    KSortFilterProxyModel {
        // 2depth tuple access
        id: servicesCountersModel
        property string filterSource
        filterRoleName: "counters"
        sourceModel: servicesCountersDataModel
        onDataChanged: data => {
                           //console.debug("countersModel changed")
                           if (filterSource){
                               tooltip = prettyCounters(icingaDataSource.data[filterSource]['counters'])
                               counterServicesOk = icingaDataSource.data[filterSource]['counters']["Ok"]
                               counterServicesWarning = icingaDataSource.data[filterSource]['counters']["Warning"]
                               counterServicesWarningHandled = icingaDataSource.data[filterSource]['counters']["Warning handled"]
                               counterServicesCritical = icingaDataSource.data[filterSource]['counters']["Critical"]
                               counterServicesCriticalHandled = icingaDataSource.data[filterSource]['counters']["Critical handled"]
                               counterServicesUnknown = icingaDataSource.data[filterSource]['counters']["Unknown"]
                               counterServicesUnknownHandled = icingaDataSource.data[filterSource]['counters']["Unknown handled"]

                           }
                       }

    }

    function prettyCounters(counters){
        var prettier = ""
        for (const prop in counters){
            prettier += prop + ": " + counters[prop] + "\n"
        }
        return prettier
    }


    fullRepresentation:FullRepresentation{
        id : full
        ErrorIcon {
            model: errorModel
        }
    }

    Plasmoid.backgroundHints: PlasmaCore.Types.DefaultBackground | PlasmaCore.Types.ConfigurableBackground
    switchWidth: Plasmoid.formFactor === PlasmaCore.Types.Planar
                 ? -1
                 : (Plasmoid.fullRepresentationItem ? Plasmoid.fullRepresentationItem.Layout.minimumWidth : Kirigami.Units.gridUnit * 8)
    switchHeight: Plasmoid.formFactor === PlasmaCore.Types.Planar
                  ? -1
                  : (Plasmoid.fullRepresentationItem ? Plasmoid.fullRepresentationItem.Layout.minimumHeight : Kirigami.Units.gridUnit * 12)
    preferredRepresentation: Plasmoid.formFactor === PlasmaCore.Types.Planar ? fullRepresentation : null
    Plasmoid.title: i18n("Icinga monitor")
    toolTipSubText: tooltip
    Plasmoid.configurationRequired: Plasmoid.configuration.icingaUrl.length === 0 || Plasmoid.configuration.icingaApiUser.length === 0 || Plasmoid.configuration.icingaApiKey.length === 0 || fetchError
    compactRepresentation: StatsCompactRepresentation {
        ErrorIcon {
            model: errorModel
        }
    }
    Component {
        /* Could be called using notifications.createNotification() instead
         * of creating a new component, but will die as soon as Notification
         * expires. So better create component passing the title as parameter
         * instead
         */
        id: notificationComponent
        Notification {
            id: notifications
            componentName: "plasma_workspace"
            eventId: "notification"
            title: "Icinga monitor"
            text: ""
            flags: Notification.CloseOnTimeout
            urgency: Notification.HighUrgency
            iconName: plasmoid.icon
            autoDelete: true
            function setTitle(text) {
                title = text
                return this
            }
            function createNotification(text) {
                // not used by now as a new component creation with title parameter
                // is used.
                console.log("creating notice " + text)
                title = text
                sendEvent()
            }
        }
    }
}
