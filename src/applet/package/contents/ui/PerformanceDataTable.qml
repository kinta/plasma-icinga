import QtQuick
import QtQuick.Layouts
import Qt.labs.qmlmodels

import QtQuick.Controls
import QtQml.Models

import org.kde.plasma.core as PlasmaCore
import org.kde.plasma.extras as PlasmaExtras
import org.kde.plasma.components as PC3

import org.kde.kquickcontrolsaddons
import org.kde.kirigami as Kirigami
import org.kde.quickcharts as Charts
import "stateresolver.js" as StateResolver
import "icinga.js" as Icinga

GridLayout{
    columns:6
    property var tableData;
    /* tableData must be the typical check output of icinga command:
       as in https://icinga.com/docs/icinga-2/latest/doc/05-service-monitoring/#performance-data-metrics
       and in https://www.monitoring-plugins.org/doc/guidelines.html#AEN201 :

       check_name=value;warnings;critical;min_value;max_value
       listed in array such as:
       [
         "available_updates=35;;;0",
         "security_updates=10;;;0"
       ]
    */

    Repeater {
        id: verticalHeader
        model:['name', ...StateResolver.IcingaStates.PerfData]

        Label  {
            width: 50
            height: 50
            text: modelData
            font.pointSize:Kirigami.Theme.smallFont.pointSize *0.7
            font.family: "monospace"
        }
    }
    Repeater{
        model:{
            let resultRows = []

            tableData.forEach((results,i)=>{
                                  let perfValues = {}

                                  results.split('=')[1].split(";").forEach((item,index) => {
                                                                               perfValues[StateResolver.IcingaStates.PerfData[index]] = item ? item : "-"
                                                                           })
                                  perfValues['name'] = results.split('=')[0];
                                  resultRows.push(perfValues)

                              })
            return resultRows;
        }
        Repeater {
            model: [ modelData.name, modelData.value, modelData.warnings, modelData.critical, modelData.min_value, modelData.max_value ]
            Label  {
                width: 50
                height: 50
                text: modelData
                font.pointSize:Kirigami.Theme.smallFont.pointSize *0.7
                font.family: "monospace"
            }
        }
    }
}
