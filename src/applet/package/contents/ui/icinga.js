function callApi(path, query, callback) {
    let xhr = new XMLHttpRequest()
    let apiUrl = `${plasmoid.configuration.icingaUrl}${path}${query}`
    let encodedAuth = Qt.btoa(`${plasmoid.configuration.icingaApiUser}:${plasmoid.configuration.icingaApiKey}`)
    let authHeader = `Basic ${encodedAuth}`

    /*if (perPage >= 1) {
        // add pagination parameter
        apiUrl += `${path.includes("?") ? "&" : "?"}per_page=${perPage}`
    }*/
    xhr.open("GET", apiUrl)
    xhr.setRequestHeader("Authorization", authHeader)
    xhr.onload = () => {
        console.log(apiUrl)
        console.log(xhr.status);
        if (xhr.status == 200) {
            try {
                let json = JSON.parse(xhr.responseText)
                if (callback) { callback(json) }
            } catch (e) {
                if (e instanceof SyntaxError) {
                    console.error(
                        `Cannot parse response for ${path} as JSON:\n` +
                        xhr.responseText
                        )
                } else { throw e }
            }
        } else {
            console.error(
                `XHR failed when retrieving ${path} (status ${xhr.status}):\n` +
                xhr.responseText
                )
        }
    }
    xhr.send()
}


function callApiPost(path, post_data, query, callback, additional_headers = []) {
    /* curl https://icinga.url:5665/v1/objects/comments -k -s -S -i -u 'user:password' -H 'Accept: application/json' \
 -H 'X-HTTP-Method-Override: GET' -X POST \
 -d '{ "joins": [ "host.name", "host.address" ], "filter": "", "pretty": true }'
*/
    console.log("BADAM")

    let xhr = new XMLHttpRequest()
    let apiUrl = `${plasmoid.configuration.icingaUrl}${path}${query}`
    let encodedAuth = Qt.btoa(`${plasmoid.configuration.icingaApiUser}:${plasmoid.configuration.icingaApiKey}`)
    let authHeader = `Basic ${encodedAuth}`

    /*if (perPage >= 1) {
        // add pagination parameter
        apiUrl += `${path.includes("?") ? "&" : "?"}per_page=${perPage}`
    }*/

    xhr.open("POST", apiUrl)
    xhr.setRequestHeader("Authorization", authHeader)
    xhr.setRequestHeader("Accept", "application/json")
    for(const header of additional_headers) {
        xhr.setRequestHeader(header[0], header[1])
    }
    //xhr.setRequestHeader("X-HTTP-Method-Override", "GET")

    xhr.onload = () => {
        console.log(apiUrl)
        console.log(xhr.status);
        if (xhr.status >= 200 && xhr.status < 300) {
            try {
                let json = JSON.parse(xhr.responseText)
                if (callback) { callback(json) }
            } catch (e) {
                if (e instanceof SyntaxError) {
                    console.error(
                        `Cannot parse response for ${path} as JSON:\n` +
                        xhr.responseText
                        )
                } else { throw e }
            }
        } else {
            console.error(
                `XHR failed when retrieving ${path} (status ${xhr.status}):\n` +
                xhr.responseText
                )
        }
    }
    console.debug(JSON.stringify(post_data));
    xhr.send(JSON.stringify(post_data))
}


function rescheduleCheck(service_id, host_name, callback) {
    const post_data = {
        "type": "Service",
        "filter": `service.name==\"${service_id}\" && service.host_name==\"${host_name}\"`,
        "force": true
    }
    callApiPost('/v1/actions/reschedule-check', post_data, "", callback)
}

function comment(comment, service_id, host_name, callback) {
    const post_data = {
        "type": "Service",
        "filter": `service.name==\"${service_id}\" && service.host_name==\"${host_name}\"`,
        "author": plasmoid.configuration.icingaApiUser,
        "comment": comment
    }
    callApiPost('/v1/actions/add-comment', post_data, "", callback)
}

function unComment(comment, service_id, host_name, callback) {
    const post_data = {
        "comment": comment
    }
    callApiPost('/v1/actions/remove-comment', post_data, "", callback)
}

function acknowledge(comment, service_id, host_name, callback) {
    const post_data = {
        "type": "Service",
        "filter": `service.name==\"${service_id}\" && service.host_name==\"${host_name}\"`,
        "author": plasmoid.configuration.icingaApiUser,
        "comment": comment
    }
    console.debug("ack sending...")
    callApiPost('/v1/actions/acknowledge-problem', post_data, "", callback)
}

function unAcknowledge(comment, service_id, host_name, callback) {
    const post_data = {
        "type":"Service",
        "filter": `service.name==\"${service_id}\" && service.host_name==\"${host_name}\"`,
        "author": plasmoid.configuration.icingaApiUser,
    }
    callApiPost('/v1/actions/remove-acknowledgement', post_data, "", callback)
}

function comments(service_id, host_name, callback) {
    const post_data = {
        "filter": `service.name==\"${service_id}\" && service.host_name==\"${host_name}\"`
    }
    callApiPost('/v1/objects/comments', post_data, "", callback, [["X-HTTP-Method-Override","GET"]])
}
