Plasma Icinga Monitor
----------------------

Plasmoid to track services monitored by icinga2 instance from your plasma desktop. It is inspired by first icinga monitor done in https://github.com/MarkusH/plasma-icinga but is adapted to plasma 6, and icinga2 .

It requires to:

- Have an Icinga2 server with its api configured-

Once what's above is there and icinga2 monitor plasmoid is placed in a panel, it will create a piechart in the panel to quick view the states of the monitored services(as done in tactical view in icingaweb2), the tooltip of that piechart will show the service states counters.

When pie chart is clicked a new panel will appear with three tabs:

- Services: A list with the services and its basic information: service name, host, zone, check time. Items has three actions:
  - More information(it will open a slidepage with raw json information about the service) and
  - Link to Icingaweb, to show the service in icingaweb (if configured the link of icingaweb in plasmoid settings)
  - Recheck, to perform a recheck of the service (it needs that user could perform these in icinga api configuration)
  Each service also has 3 tabs:
    - The check command output.
    - The performance data gauges and table.
    - The comments tab that allows to add acknowledgements and comments for the issue.
  Items can be searched by name, and grouped by states, by host name, or by zone.
- Hosts: super basic as in my case it's rarely used. It will just list the hosts, without any information(PR are welcome)
- Tactical: A Pie chart with the states of the services and a legend with the counters of the state. 

## What's new in plasma6 version?

- Allows comments and acknowledges in services issues.
- Adds performance data gauges and tables to quickly view the issues.
- Show the quantity of critical issues in compact representation.

## Build and install instructions

Restart plasma to load the applet 
(in a terminal type: 
`kquitapp plasmashell`
and then
`plasmashell`)

or view it with 
`plasmoidviewer -a icinga_monitor`

It could be easily debugged launching within qtcreator.
In left bar pick projects >> and in run settings:

- Executable field: `/usr/bin/plasmoidviewer`
- Command line arguments: `-a .  -l topedge  -x 0 -y 0 -s 800x600`
- Working directory: `%{ActiveProject:Path}/package`

It also could be attached to debug in qtcreator

`plasmoidviewer --applet org.communia.plasma.icinga_monitor --qmljsdebugger port:1234`


## Tutorials and resources about plasmoiding

https://techbase.kde.org/Development/Tutorials/Plasma5/QML2/GettingStarted

Plasma QML API explained
https://techbase.kde.org/Development/Tutorials/Plasma2/QML2/API


## TODO: 

- chartview from services model.
- hosts from models
- notifications?
- Issues
- Include certain  states only filters.


