// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2020 Aleix Quintana Alsius <kinta@communia.org>
#ifndef ICINGADATAENGINE_H
#define ICINGADATAENGINE_H

#include <Plasma5Support/DataEngine>
#include <Plasma5Support/Service>

#include <QNetworkAccessManager>
//#include <QNetworkConfigurationManager>
//#include <QNetworkInformation>
#include <QLoggingCategory>

#include <KConfig>
#include <KConfigGroup>



#include <QJsonDocument>

#include <servicelistmodel.h>


class QDateTime;

struct FetchResult {
    bool result;
    QString error;
    QString source; 
};

/**
 * This data engine will allow running it via service or automatically
 * reading configuration from icingarc. 
 * 
 * In case of service:
 * Request "Service" from "services" source, then make a "fetch" 
 * operation. It wil need  setting icinga url, user, and key(token).
 * If everything is fine it will create a new source with the name set
 * to the endpoint url. From there a lot of information can be processed,
 * also a Model can be obtained, in that model the endpoint data items are
 * obtained.
 * 
 * In case of reading configuration from icingarc (placed in standard config dirs):
 * It will read the entries from a config file such as:
 * 
 * cat .config/icingarc:
 * [General]
 * url=https://icinga.org:5665
 * apiuser=myuser
 * apikey=blablabla
 * 
 * Then it will automatically fetch the services endpoint from that url.
 * If everything is fine it will create a new source with the name set
 * to the endpoint url. From there a lot of information can be processed,
 * also a Model can be obtained, in that model the endpoint data items are
 * obtained.
 */
class IcingaDataEngine : public Plasma5Support::DataEngine
{
    Q_OBJECT

public:
    enum ServiceStates {
        Ok,
        Warning,
        Critical,
        Unknown,
        WarningHandled,
        CriticalHandled,
        UnknownHandled,
    };
    Q_ENUM(ServiceStates);
    
    enum HostStates {
        Up,
        Down,
        Unreachable,
        DownHandled,
        UnreachableHandled       
    };
    Q_ENUM(HostStates);

    IcingaDataEngine(QObject *parent, const QVariantList& args);
    ~IcingaDataEngine();
    Plasma5Support::Service* serviceForSource(const QString& source) override;
    /**
     * The service way to update a source (the endpoint of a icinga api)
     * such as services, hosts... By now only services is done.
     * 
     * @param source As in icinga api endpoint such as "services", "hosts".
     * @param icingaUrl The Api Url.
     * @param icingaApiUser The user name.
     * @param icingaApiKey Icinga API token.
     * 
     * @return QPair if the update can be done or not, and the error string if any
     */
     FetchResult fetch(const QString& source, const QString& icingaUrl, const QString& icingaApiUser, const QString& icingaApiKey, const QString& endpoint); 

protected:
    bool sourceRequestEvent(const QString &name) override;

protected Q_SLOTS:
    /**
     * To update sourceEvent whenever it is carrying on reading config 
     * from icingarc.
     * 
     *  @param source The source, mapped from icinga api endpoint such as "services", "hosts".
     * 
     *  @return if the update can be done or not.
     */
    bool updateSourceEvent(const QString &source) override;

private Q_SLOTS:
    void networkStatusChanged();
    Data interpretData(const QJsonObject result);
    int stateToInt(const QString state);
    void updateData();
    void networkDown();
    void networkUp();

private:
    QNetworkAccessManager m_manager;
    QJsonDocument m_jdoc;
    QString m_name;
    QUrl m_url;
    QString m_apiuser;
    QString m_apikey;

    KConfig m_config;
    KConfigGroup m_generalcg;
    QDateTime m_lastUpdate;
    bool m_hasNetwork;
    ServiceListModel *m_services;
    friend class ServicesAction;
    
};


Q_DECLARE_LOGGING_CATEGORY(ICINGAENGINE);

#endif // ICINGADATAENGINE_H
