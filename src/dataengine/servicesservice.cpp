// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2020 Aleix Quintana Alsius <kinta@communia.org>
#include "servicesservice.h"
#include "icingadataengine.h"
#include "servicesaction.h"
#include <Plasma5Support/ServiceJob>

#include "icingaenginedebug.h"

ServicesService::ServicesService(IcingaDataEngine* parent, const QString& source)
: Plasma::Service(parent),
m_icingaDataEngine(parent)
{
    qCDebug(ICINGAENGINE) << "constructing servicesService";
    setName(QStringLiteral("icinga-services"));
    setDestination(source);

}

Plasma::ServiceJob* ServicesService::createJob(const QString& operation, QMap<QString, QVariant>& parameters)
{
    qCDebug(ICINGAENGINE) << "creating Service JOB";
    parameters["endpoint"] = "/v1/objects/services";
        return new ServicesAction(m_icingaDataEngine, destination(), operation, parameters, this);
}
