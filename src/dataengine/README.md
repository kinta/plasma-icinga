# About the ICINGA DATAENGINE
This data engine will allow running it via service or automatically
reading configuration from icingarc. 

## In case of service:
Request "Service" from "services" source, then make a "fetch" 
operation. It wil need  setting icinga url, user, and key(token).
If everything is fine it will create a new source with the name set
to the endpoint url. From there a lot of information can be processed,
also a Model can be obtained, in that model the endpoint data items are
obtained.

## In case of global configuration 
Reading configuration from icingarc (placed in standard config dirs):
It will read the entries from a config file such as:

```
cat .config/icingarc:
[General]
url=https://icinga.org:5665
apiuser=myuser
apikey=blablabla
```

Then it will automatically fetch the services endpoint from that url.
If everything is fine it will create a new source with the name set
to the endpoint url. From there a lot of information can be processed,
also a Model can be obtained, in that model the endpoint data items are
obtained.

## Deal with self signed certs.


As normally icinga runs with self-signed certificates, copy the ca certificate of api endpoint to where you want to connect normally in :

    cat /var/lib/icinga2/ca/ca.crt

Append to /etc/ca-certificates/trust-source/anchors/ :
    
    # vim  /etc/ca-certificates/trust-source/anchors/ca-remote-api-icinga2.crt
    # update-ca-trust

If not running arch or fedora, maybe you'll need to check with other ways as in https://wiki.archlinux.org/index.php/User:Grawity/Adding_a_trusted_CA_certificate


# About the applet
This datengine allows the use of a plasmoid that show the services (allowing to filter them). Basic Host list (only names). Tactical view: a graph to quick show the state of the services.

