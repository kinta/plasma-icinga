// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2020 Aleix Quintana Alsius <kinta@communia.org>
#ifndef SERVICESACTION_H
#define SERVICESACTION_H

#include "icingadataengine.h"

#include <Plasma5Support/ServiceJob>
namespace Plasma = Plasma5Support;

/**
 * @todo write docs
 */
class ServicesAction : public Plasma::ServiceJob
{
    Q_OBJECT

    public:
        /**
        * Default constructor
        */
        ServicesAction(IcingaDataEngine* engine,
                           const QString& destination,
                           const QString& operation,
                           QVariantMap& parameters,
                           QObject* parent = nullptr)
        : ServiceJob(destination, operation, parameters, parent),
          m_engine(engine)
        {
        }

        void start() override;

    private:
        IcingaDataEngine* m_engine;
};

#endif // SERVICESACTION_H
