// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2020 Aleix Quintana Alsius <kinta@communia.org>
#include "servicelistmodel.h"

#include <KLocalizedString>
#include <QDebug>

#include "icingaenginedebug.h"

ServiceListModel::ServiceListModel(QObject *parent) :
    QAbstractListModel(parent)
{
}

ServiceListModel::ServiceListModel(const ServiceItem &item, QObject *parent): QAbstractListModel(parent){
    m_services.append(item);
}

ServiceListModel::~ServiceListModel()
{}

int ServiceListModel::rowCount(const QModelIndex &index) const
{
    if (!index.isValid()) {
        return m_services.size();
    }

    return 0;
}

QHash<int, QByteArray> ServiceListModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles.insert(Qt::DisplayRole, "display");
    roles.insert(Qt::DecorationRole, "decoration");
    roles.insert(ServiceListModel::DisplayNameRole, "displayName");
    roles.insert(ServiceListModel::ServiceIdRole, "serviceId");
    roles.insert(ServiceListModel::HostNameRole, "hostName");
    roles.insert(ServiceListModel::LastCheckOutputRole, "lastCheckOutput");
    roles.insert(ServiceListModel::ServiceStateRole, "serviceState");
    roles.insert(ServiceListModel::ZoneRole, "zone");
    roles.insert(ServiceListModel::RawRole, "raw");
    return roles;
}


QVariant ServiceListModel::data(const QModelIndex &index, int role) const
{
    //qDebug()<< "data asked for" << index.row() << "and role" << role;
    if (!index.isValid() || index.row() >= m_services.size() || index.row() < 0 ) {
        return QVariant();
    }
    //qDebug() << " Data asked for " << index.row() << " and role " << role;
    const ServiceItem& item = m_services.at(index.row());

    switch (role) {
        case Qt::DisplayRole: {
            return i18nc("(id)",
                         "[STATUS] %1 (%2) - (%3)", item.displayName, item.id, item.hostName);
        } 
        case DisplayNameRole: {
            return item.displayName;
        }
        
        case ServiceIdRole: {
            return item.id;
        }
        case HostNameRole: {
            return item.hostName;
        }
        case LastCheckOutputRole: {
            return item.lastCheckOutput;
        }
        case ServiceStateRole: {
            return item.state;
        }
        case ZoneRole: {
            return item.zone;
        }
        case RawRole: {
            return item.raw;
        }
        default:
            return QVariant();
    }

    return QVariant();
}

bool ServiceListModel::setData(const QModelIndex& index, const QVariant& value, int role )
{
    if (!index.isValid() || value.isNull()) {
        return false;
    }

    return false;
}

void ServiceListModel::append(const ServiceItem &item) {
    
    //beginInsertRows(QModelIndex(), i, i);
    beginInsertRows(QModelIndex(), m_services.count(), m_services.count());
    m_services.append(item);
    // Emit changed signals
    //qDebug() << "append at pos " << i << " now m_services is " << m_services.count();
    emit countChanged();
    endInsertRows();
}

void ServiceListModel::append(const QList<ServiceItem> &items)
{
    if(items.count() == 0) return;
    beginInsertRows(QModelIndex(), m_services.count(), m_services.count()+items.count()-1);
    m_services << items;
    endInsertRows();
    emit countChanged();
}

void ServiceListModel::appendOrReplace(int i, const ServiceItem &item)
{
    if(i < 0) return;
    //qCDebug(ICINGAENGINE) << "Append or replace at " << i << " When size is " << m_services.size();
    if (m_services.size() -1  < i ) {
        //qCDebug(ICINGAENGINE) << "appending"; 
        append(item);
    } else {
        //qCDebug(ICINGAENGINE) << "replacing"; 
        m_services.replace(i, item);
        emit dataChanged(index(i, 0) , index(i, 0));
    }
    // Emit changed signals
}

void ServiceListModel::insert(const ServiceItem &item, int i)
{
    beginInsertRows(QModelIndex(), i, i);
    m_services.insert(i, item);

    // Emit changed signals
    emit countChanged();

    endInsertRows();
}

void ServiceListModel::insert(const QVector<ServiceItem> &items, int index)
{
    if(items.count() == 0) return;
    index = qMax(0, index);
    Q_ASSERT(index >= m_services.count());

    beginInsertRows(QModelIndex(), index, index+items.count());
    for(int i = 0, k = index; i < items.count(); ++i, ++k)
        m_services.insert(k, items.at(i));
    endInsertRows();
    emit countChanged();
}

/*bool ServiceListModel::remove(const ServiceItem &item)
{
    int row = m_services.indexOf(item);
    if(row < 0) return false;

    beginRemoveRows(QModelIndex(), row, row);
    m_services.removeAt(row);
    endRemoveRows();
    emit countChanged();
    return true;
}*/

void ServiceListModel::removeAt(int index)
{
    if(index < 0 || index >= m_services.count()) return;

    beginRemoveRows(QModelIndex(), index, index);
    m_services.removeAt(index);
    endRemoveRows();
    emit countChanged();
}

void ServiceListModel::trim(int index)
{
    if(index < 0 || index >= m_services.count()) return;

    beginRemoveRows(QModelIndex(), index, m_services.count()-1);
    for (int row = index ; row <  m_services.count()-1; ++row) {
      m_services.removeAt(index);
    }
    endRemoveRows();
    emit countChanged();
}

void ServiceListModel::clear()
{
    beginResetModel();
    m_services.clear();
    //qDeleteAll(*m_services);
    endResetModel();
}

int ServiceListModel::count() const
{
    return m_services.count();
}

