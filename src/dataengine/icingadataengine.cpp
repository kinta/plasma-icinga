// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2020 Aleix Quintana Alsius <kinta@communia.org>
#include "icingadataengine.h"
#include "servicelistmodel.h"
#include "servicesservice.h"

#include <QtCore/QDateTime>
#include <QNetworkInformation>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkRequest>

#include "icingaenginedebug.h"

//#include <Plasma5Support/DataEngine>

namespace
{
bool isOnline()
{
    return QNetworkInformation::instance() && QNetworkInformation::instance()->reachability() == QNetworkInformation::Reachability::Online;
}
} // namespace


IcingaDataEngine::IcingaDataEngine(QObject *parent, const QVariantList &args)
: Plasma5Support::DataEngine(parent),
m_manager(parent),
m_jdoc(),
m_name(),
m_config("icingarc"),
m_lastUpdate()
{
    // We ignore any arguments - data engines do not have much use for them
    Q_UNUSED(args)
    // We can make use of icingarc in config dir or request data via sourceService.
    m_generalcg = m_config.group("General");
    /** Url for icinga2 api and credentials as set in /etc/icinga2/conf.d/api-users.conf **/
    m_url = m_generalcg.readEntry("url", QString());
    m_apiuser = m_generalcg.readEntry("apiuser", QString() );
    m_apikey = m_generalcg.readEntry("apikey", QString());
    m_services = new ServiceListModel(this);
    QNetworkInformation::loadBackendByFeatures(QNetworkInformation::Feature::Reachability | QNetworkInformation::Feature::TransportMedium);
    m_hasNetwork = QNetworkInformation::instance() && QNetworkInformation::instance()->reachability() == QNetworkInformation::Reachability::Online;
    if (auto info = QNetworkInformation::instance()) {
        connect(info, &QNetworkInformation::reachabilityChanged, this, &IcingaDataEngine::networkStatusChanged);
        connect(info, &QNetworkInformation::transportMediumChanged, this, &IcingaDataEngine::networkStatusChanged);
    } else {
        qWarning() << "QNetworkInformation has no backend. Is NetworkManager.service running?";
    }

    if (!m_url.isEmpty() && !m_apiuser.isEmpty() && !m_apikey.isEmpty()){
        setMinimumPollingInterval(5000);
        //Automatic requesting services
        updateSourceEvent("services");
    }
}
IcingaDataEngine::~IcingaDataEngine() {
}

bool IcingaDataEngine::sourceRequestEvent(const QString &name)
{
    qCDebug(ICINGAENGINE) << "sourceRequestEvent" << name;
    return updateSourceEvent(name);
}

bool IcingaDataEngine::updateSourceEvent(const QString &name)
{
    qCDebug(ICINGAENGINE) << "New updateSourceEvent  for source \"" << name << "\" expects running directly requires icingarc";
    if (!m_url.isEmpty() && !m_apiuser.isEmpty() && !m_apikey.isEmpty()){
        qCDebug(ICINGAENGINE) << "going on with updateSourceEvent for source " << name << ", icingarc found";
        if (name == "services"){
            return fetch(name, m_url.toString(), m_apiuser, m_apikey, "/v1/objects/services").result;
        }
    }
    qCDebug(ICINGAENGINE) << "No credentials found in icingarc do not update anything for source " << name << " maybe will run via serviceforsource later";
    return false;
}

FetchResult IcingaDataEngine::fetch(const QString &name, const QString &icingaUrl, const QString &icingaApiUser, const QString &icingaApiKey, const QString &endpoint)
{
    //m_hasNetwork = m_network_information.reachability() == QNetworkInformation::Reachability::Online;
    m_url = icingaUrl;
    
    m_url.setPath(endpoint);
    
    qCDebug(ICINGAENGINE) << "Fetching " << name;
    if (isOnline()) {
        setData(m_url.host(), "errorMessage", "");
        if (name == "services") {
            QString concat_user_key = icingaApiUser + ":" + icingaApiKey;
            QByteArray enc_concat_user_key = concat_user_key.toLocal8Bit().toBase64();
            QString header_auth =  "Basic " + enc_concat_user_key;
            QNetworkRequest req(m_url);
            req.setRawHeader("Authorization", header_auth.toLocal8Bit());
            QNetworkReply *reply = m_manager.get(req);
            setData(m_url.toString(), "connected", m_hasNetwork);
            if (modelForSource(m_url.toString()) == nullptr || !sources().contains(m_url.toString()) ) {
                qCDebug(ICINGAENGINE) << "Setting model to services list #" << m_services->count() ;
                setModel(m_url.toString(), m_services);
            }
            connect(reply, SIGNAL(finished()), this, SLOT(updateData()));
        } else {
            setData(m_url.toString(), "errorMessage", "Endpoint not implemented");
            qCDebug(ICINGAENGINE) << "Not Implemented.";
            return {false, "Endpoint not implemented", m_url.toString()};
        }
    } else {
        setData(m_url.host(), "errorMessage", "Not connected to a network");
        if (m_url.host() != m_url.toString()){
            setData(m_url.toString(), "errorMessage", "Not connected to a network");
            if (modelForSource(m_url.toString()) != nullptr) {
                qCDebug(ICINGAENGINE) << "Clear to force refresh source: " << m_url.toString();
                m_services->clear();
            }
        }
        qCDebug(ICINGAENGINE) << "No connection";
        return {false, "No connection", m_url.toString()};
    }
    return {true, "", m_url.toString()};;
}

void IcingaDataEngine::updateData()
{
    qCDebug(ICINGAENGINE) << "updateData";
    QNetworkReply *reply = static_cast<QNetworkReply*>(sender());
    if (reply->error() == QNetworkReply::NoError) {
        //qCDebug(ICINGAENGINE) << "updateData: connected so no network error";
        QByteArray data = reply->readAll();
        int http_status_code = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
        // qCDebug(ICINGAENGINE) << "getDone" << http_status_code << data.size() << m_url;
        QJsonParseError ok;
        QJsonObject result = m_jdoc.fromJson(data, &ok).object();
        m_lastUpdate = QDateTime::currentDateTime();
        if (ok.error != QJsonParseError::NoError ) {
            qCDebug(ICINGAENGINE) << "Error parsing the json data";
            setData(m_url.toString(), "errorMessage", reply->errorString());
        } else {
            // Succesful, set the 
            setData(m_url.toString(), "errorMessage", "");
            setData(m_url.toString(), interpretData(result));
        }
    } else {
        // Error set the services source info
        qCDebug(ICINGAENGINE) << "updateData: network error";
        setData(m_url.toString(), "errorMessage", reply->errorString());
    }
    reply->deleteLater();
}

Plasma::DataEngine::Data IcingaDataEngine::interpretData(const QJsonObject result)
{
    qCDebug(ICINGAENGINE) << "interpreteData";
    Plasma::DataEngine::Data data;
    data["version"] = result["cgi_json_version"];
    data["last-update"] = m_lastUpdate;
    
    QJsonValue service_status = result["results"];
    QList<QVariant> list = service_status.toVariant().toList();
    QMap<QString, QVariant> map = list[0].toMap();
    data["services-count"] = list.count();
    
    
    int stateType;
    int state[] = {0, 0, 0, 0, 0, 0, 0, 0}; // OK, WRN, CRIT, UNK, WRN-HNDL, CRIT-HNDL, UNK-HNDL, REALSTATUS
    int serviceState;
    QVariantMap service;
    
    QVariantList hostlist; // move to hostlist
    QVariantMap counter; 
    
    int service_i = 0;
    for (const QVariant &_service : qAsConst(list)) {
        //foreach (QVariant _service, list) {
        service = _service.toMap();
        QMap<QString, QVariant> attrs = service["attrs"].toMap();
        QMap<QString, QVariant> last_check_result = attrs["last_check_result"].toMap();
        
        
        if (!hostlist.contains(attrs["host_name"].toString())){
            hostlist.append(attrs["host_name"].toString());
        }
        
        stateType = last_check_result["state"].toInt(); //stateToInt(last_check_result["stat"].toString());
        
        if (stateType > state[ServiceStates::Unknown] && (attrs["handled"].toBool())) {//attrs["downtime_depth"].toBool() || attrs["acknowledgement"].toBool())) {
            serviceState = stateType + 3;
            state[stateType + 3]++;
            //qCDebug(ICINGAENGINE) <<  attrs["host_name"] << attrs["display_name"] << "added to " << stateType+3 ;
        } else {
            serviceState = stateType;
            state[stateType]++;
            //qCDebug(ICINGAENGINE) << "[HANDLED]" << attrs["host_name"] << attrs["display_name"] << "added to " << stateType ;
        }
        // To debug in qml side set the raw property
        ServiceItem msg = ServiceItem(
            attrs["__name"].toString(),
                                      attrs["display_name"].toString(),
                                      attrs["host_name"].toString(),
                                      last_check_result["output"].toString(),
                                      serviceState,
                                      attrs["zone"].toString(),
                                      attrs
        );
        m_services->appendOrReplace(service_i,msg);
        service_i++;
    }
    m_services->trim(service_i);
    data["hostsCount"] = hostlist.count();
    data["hosts"] = hostlist;
    
    counter["Ok"] = state[ServiceStates::Ok];
    counter["Warning"] = state[ServiceStates::Warning];
    counter["Critical"] = state[ServiceStates::Critical];
    counter["Unknown"] = state[ServiceStates::Unknown];
    counter["Warning handled"] = state[ServiceStates::WarningHandled];
    counter["Critical handled"] = state[ServiceStates::CriticalHandled];
    counter["Unknown handled"] = state[ServiceStates::UnknownHandled];
    data["counters"] = QVariant::fromValue(counter);
    //Whenever counter charts are delegated as it should via model and not looking up traversing arrays, then counters can be removed, and next will be useful
    data["counterOk"] = state[ServiceStates::Ok];
    data["counterWarning"] = state[ServiceStates::Warning];
    data["counterCritical"] = state[ServiceStates::Critical];
    data["counterUnknown"] = state[ServiceStates::Unknown];
    data["counterWarningHandled"] = state[ServiceStates::WarningHandled];
    data["counterCriticalHandled"] = state[ServiceStates::CriticalHandled];
    data["counterUnknownHandled"] = state[ServiceStates::UnknownHandled];
    qCDebug(ICINGAENGINE) << "Model services feed with services count: #" << m_services->count() ;
    return data;
}

void IcingaDataEngine::networkDown()
{
    qCDebug(ICINGAENGINE) << "networkDown";
    m_hasNetwork = false;
}

void IcingaDataEngine::networkUp()
{
    //qCDebug(ICINGAENGINE) << "networkUp";
    m_hasNetwork = true;
}

void IcingaDataEngine::networkStatusChanged()
{
    qCDebug(ICINGAENGINE) << "network status changed ";
    if (isOnline()) {
        // start updating the endpoint
        for(const auto& endpoint: sources()) {
            qCDebug(ICINGAENGINE) << "Reupdating source " << endpoint;
            updateSourceEvent(endpoint);
            //networkUp();
        }
    } else {
        networkDown();
    }
}

int IcingaDataEngine::stateToInt(const QString state)
{
    if (state == "OK") {
        return 0;
    } else if (state == "WARNING") {
        return 1;
    } else if (state == "CRITICAL") {
        return 2;
    }
    return 3; // UNKNOWN
}

Plasma::Service *IcingaDataEngine::serviceForSource(const QString &source)
{
    //qCDebug(ICINGAENGINE) << "Requesting service from " << source;
    if (source == "services"){
        return new ServicesService(this, source);
        
    }
    
    return Plasma::DataEngine::serviceForSource(source);
}


//K_EXPORT_PLASMA_DATAENGINE_WITH_JSON(icinga, IcingaDataEngine, "plasma-dataengine-icinga.json");
K_PLUGIN_CLASS_WITH_JSON(IcingaDataEngine, "plasma-dataengine-icinga.json")
Q_LOGGING_CATEGORY(ICINGAENGINE, "icinga")


#include "icingadataengine.moc"
