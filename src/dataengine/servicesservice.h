// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2020 Aleix Quintana Alsius <kinta@communia.org>
#ifndef SERVICESSERVICE_H
#define SERVICESSERVICE_H

#include <Plasma5Support/Service> 
namespace Plasma = Plasma5Support;

class IcingaDataEngine;

class ServicesService :  public Plasma::Service
{
    Q_OBJECT
    
public:
    ServicesService(IcingaDataEngine* parent, const QString& source);
    
protected:
    virtual Plasma::ServiceJob* createJob(const QString& operation, QMap<QString, QVariant> &parameters) override;
    
private:
    IcingaDataEngine* m_icingaDataEngine;
    
};

#endif // SERVICESSERVICE_H
