// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2020 Aleix Quintana Alsius <kinta@communia.org>

#include "servicesaction.h"

#include <klocalizedstring.h>

#include "icingaenginedebug.h"

void ServicesAction::start()
{
    //qCDebug(ICINGAENGINE) << "Trying to perform the action " << operationName() << " on " << destination();
    qCDebug(ICINGAENGINE) << "actionId: " << parameters()["actionId"].toString();
    //qCDebug(ICINGAENGINE) << "params: " << parameters();

    if (!m_engine) {
        setErrorText(i18n("The icinga dataEngine is not set."));
        setError(-1);
        emitResult();
        return;
    }

    const QStringList dest = destination().split(' ');

    uint id = 0;
    if (dest.count() >  1 && !dest[1].toInt()) {
        setErrorText(i18n("Invalid destination: %1", destination()));
        setError(-2);
        emitResult();
        return;
    } else if (dest.count() >  1) {
        id = dest[1].toUInt();
    }
    if (operationName() == QLatin1String("fetchServices")) {

        QVariantMap hints;
        if (parameters().value(QStringLiteral("skipGrouping")).toBool()) {
            hints.insert(QStringLiteral("x-kde-skipGrouping"), true);
        }
        qCDebug(ICINGAENGINE) << "Update Source from Service" << destination();
        FetchResult rv = m_engine->fetch(destination(), parameters().value(QStringLiteral("icingaUrl")).toString(),
                                              parameters().value(QStringLiteral("icingaApiUser")).toString(),
                                              parameters().value(QStringLiteral("icingaApiKey")).toString(),
                                              parameters().value(QStringLiteral("endpoint")).toString());
        setResult(rv.result);
        setError(rv.result);
        setErrorText(rv.error);
        setProperty("source",rv.source); // Not usable in qml
        setResult(rv.result); // calls emitResult internally.
        return;
    } 
    emitResult();

}
